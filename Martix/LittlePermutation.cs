﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrix
{
    class LittlePermutation
    {
        List<List<int>> list;
        AdjMatrix a;
        AdjMatrix b;
        int[] eachDegree_a;
        string[] eachNearInfo_a;
        string[] eachSmToInfo_a;
        string[] eachPathInfo_a;

        int[] eachDegree_b;
        string[] eachNearInfo_b;
        string[] eachSmToInfo_b;
        string[] eachPathInfo_b;

        public List<List<int>> GenerateFullPermutation(int[] array, AdjMatrix a, AdjMatrix b)
        {
            this.a = a;
            this.b = b;
            list = new List<List<int>>();
            InitEachInfo();
            GenerateFullPermutation(array, 0);
            
            return list;
        }

        void InitEachInfo()
        {
            eachDegree_a = a.GetEachDegree();
            eachNearInfo_a = a.GetEachNearInfo();
            eachPathInfo_a = a.GetEachPathInfo();
            eachSmToInfo_a = a.GetEachSmToInfo();

            eachDegree_b = b.GetEachDegree();
            eachNearInfo_b = b.GetEachNearInfo();
            eachPathInfo_b = b.GetEachPathInfo();
            eachSmToInfo_b = b.GetEachSmToInfo();
        }

        private void GenerateFullPermutation(int[] array, int index)
        {
            if (index >= array.Length)
            {
                List<int> lt = new List<int>();
                for (int i = 0; i < array.Length; i++)
                {
                    lt.Add(array[i]);
                }
                list.Add(lt);

                return;
            }

            //int id = array[index];
            for (int j = index; j < array.Length; j++)
            {
                //int id = array[index];
                int temp_id = array[j];
                //Console.WriteLine("{0} yu {1}", index, temp_id);
                //Console.WriteLine("j: {0}", j);
                
                if (eachDegree_a[index] != eachDegree_b[temp_id]
                    || eachNearInfo_a[index] != eachNearInfo_b[temp_id]
                    || eachPathInfo_a[index] != eachPathInfo_b[temp_id]
                    || eachSmToInfo_a[index] != eachSmToInfo_b[temp_id]
                    )
                {
                    continue;
                }

                array = Swap(array, j, index);
                GenerateFullPermutation(array, index + 1);
                array = Swap(array, j, index);
            }
        }

        private int[] Swap(int[] array, int i, int j)
        {
            if (i != j)
            {
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }

            return array;
        }
    }
}

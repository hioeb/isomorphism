﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrix
{
    class Program
    {
        static void Main(string[] args)
        {
            // 广度优先搜索测试
            //AdjMatrix adj = new AdjMatrix(6, false);
            //adj.AddEdge(0, 1);
            //adj.AddEdge(0, 2);
            //adj.AddEdge(0, 5);
            //adj.AddEdge(2, 3);
            //adj.AddEdge(2, 4);

            //BreadthFirstSearch bfs = new BreadthFirstSearch(adj, 0);
            //for (int i = 0; i < 6; i++)
            //{
            //    Console.WriteLine(bfs.PathTo(i));
            //    Console.WriteLine(bfs.PathLenTo(i));
            //}

            //
            Console.WriteLine("^^^^^^^^^^^^^^^^test2^^^^^^^^^^^^^^^^");
            AdjMatrix a_1 = new AdjMatrix(4, false);
            a_1.AddEdge(0, 1);
            a_1.AddEdge(1, 2);
            a_1.AddEdge(2, 3);
            a_1.AddEdge(3, 0);
            Console.WriteLine("------------------a----------------");
            string m_a_1 = a_1.ToString();
            Console.WriteLine(m_a_1);

            AdjMatrix b_1 = new AdjMatrix(4, false);
            b_1.AddEdge(3, 1);
            b_1.AddEdge(1, 0);
            b_1.AddEdge(0, 2);
            b_1.AddEdge(2, 3);
            Console.WriteLine("------------------b----------------");
            string m_b_1 = b_1.ToString();
            Console.WriteLine(m_b_1);

            a_1.IsoEqual(b_1);

            Console.WriteLine("^^^^^^^^^^^^^^^^test1^^^^^^^^^^^^^^^^");
            AdjMatrix a = new AdjMatrix(6, false);
            a.AddEdge(0, 1);
            a.AddEdge(0, 5);
            a.AddEdge(1, 2);
            a.AddEdge(1, 4);
            a.AddEdge(5, 2);
            a.AddEdge(5, 4);
            a.AddEdge(3, 2);
            a.AddEdge(3, 4);
            Console.WriteLine("------------------a----------------");
            string m_a = a.ToString();
            Console.WriteLine(m_a);

            AdjMatrix b = new AdjMatrix(6, false);
            b.AddEdge(0, 1);
            b.AddEdge(0, 5);
            b.AddEdge(1, 5);
            b.AddEdge(1, 2);
            b.AddEdge(5, 4);
            b.AddEdge(2, 4);
            b.AddEdge(3, 2);
            b.AddEdge(3, 4);
            Console.WriteLine("------------------b----------------");
            string m_b = b.ToString();
            Console.WriteLine(m_b);

            bool result;
            result = a.IsoEqual(b);
            Console.WriteLine("Match ? " + result);

            Console.WriteLine("^^^^^^^^^^^^^^^^test3^^^^^^^^^^^^^^^^");
            AdjMatrix a_2 = new AdjMatrix(9, false);
            a_2.AddEdge(0, 1);
            a_2.AddEdge(1, 2);
            a_2.AddEdge(2, 3);
            a_2.AddEdge(3, 4);
            a_2.AddEdge(4, 5);
            a_2.AddEdge(5, 6);
            a_2.AddEdge(6, 7);
            a_2.AddEdge(7, 8);
            a_2.AddEdge(8, 0);
            a_2.AddEdge(1, 8);
            a_2.AddEdge(2, 7);

            Console.WriteLine("------------------a----------------");
            string m_a_2 = a_2.ToString();
            Console.WriteLine(m_a_2);

            AdjMatrix b_2 = new AdjMatrix(9, false);
            b_2.AddEdge(1, 0);
            b_2.AddEdge(0, 8);
            b_2.AddEdge(8, 7);
            b_2.AddEdge(7, 6);
            b_2.AddEdge(6, 5);
            b_2.AddEdge(5, 4);
            b_2.AddEdge(4, 3);
            b_2.AddEdge(3, 2);
            b_2.AddEdge(2, 1);
            b_2.AddEdge(0, 2);
            b_2.AddEdge(3, 8);

            Console.WriteLine("------------------b----------------");
            Console.WriteLine(b_2.ToString());

            bool result_2;
            result_2 = a_2.IsoEqual(b_2);

            Console.WriteLine("SM2------------b");
            BreadthFirstSearch bfs_1 = new BreadthFirstSearch(b_2, 0);
            for (int i = 0; i < b_2.GetVertexCount(); i++)
            {
                Console.WriteLine(bfs_1.PathTo(i));
                Console.WriteLine(bfs_1.PathLenTo(i));
            }

            Console.WriteLine("SM2------------a");
            BreadthFirstSearch bfs = new BreadthFirstSearch(a_2, 0);
            for (int i = 0; i < a_2.GetVertexCount(); i++)
            {
                Console.WriteLine(bfs.PathTo(i));
                Console.WriteLine(bfs.PathLenTo(i));
            }

            //b_2.ExchangeColumn(0, 1);
            //b_2.ExchangeRow(0, 1);

            //b_2.ExchangeColumn(2, 8);
            //b_2.ExchangeRow(2, 8);

            //b_2.ExchangeColumn(3, 7);
            //b_2.ExchangeRow(3, 7);

            //b_2.ExchangeColumn(4, 6);
            //b_2.ExchangeRow(4, 6);

            //b_2.ExchangeColumn(5, 5);
            //b_2.ExchangeRow(5, 5);

            //Console.WriteLine(b_2.ToString());
            //Console.WriteLine(a_2.Equals(b_2));
        }
    }
}

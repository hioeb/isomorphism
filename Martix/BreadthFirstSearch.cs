﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrix
{
    class BreadthFirstSearch
    {
        private AdjMatrix adj;
        private bool[] visited;
        private int[] edgeTo;
        private int s;

        public BreadthFirstSearch(AdjMatrix adj, int s)
        {
            this.adj = adj;
            this.visited = new bool[adj.GetVertexCount()];
            this.edgeTo = new int[adj.GetVertexCount()];
            this.s = s;
            bfs();
        }

        // 广度优先搜索
        private void bfs()
        {
            Queue<int> process = new Queue<int>();

            visited[s] = true;
            process.Enqueue(s);

            int v;
            int[,] a = adj.GetMatrix();
            while (process.Count != 0)
            {
                v = process.Dequeue();

                for (int j = 0; j < adj.GetVertexCount(); j++)
                {
                    if (a[v, j] != 0 && !visited[j])
                    {
                        edgeTo[j] = v;
                        visited[j] = true;
                        process.Enqueue(j);
                    }
                }
            }
        }

        // 两个节点间是否存在通路
        public bool HavePathTo(int v)
        {
            return visited[v];
        }

        // 到某节点的路径
        public string PathTo(int v)
        {
            if (!this.HavePathTo(v))
            {
                return "";
            }

            Stack<int> path = new Stack<int>();
            int t = v;
            while (t != s)
            {
                path.Push(t);
                t = edgeTo[t];
            }
            path.Push(s);

            StringBuilder sb = new StringBuilder();
            while (path.Count != 0)
            {
                sb.Append(path.Pop().ToString() + "->");
            }
            sb.Remove(sb.Length - 2, 2);

            return sb.ToString();
        }

        // 到某节点的距离
        public int PathLenTo(int v)
        {
            if (!this.HavePathTo(v))
            {
                return 0;
            }

            Stack<int> path = new Stack<int>();
            int t = v;
            while (t != s)
            {
                path.Push(t);
                t = edgeTo[t];
            }
            //path.Push(s);

            return path.Count;
        }
    }
}

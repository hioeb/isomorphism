﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrix
{
    // Adjacency Matrix 邻接矩阵
    class AdjMatrix
    {
        int vertex_count; // 顶点数
        int edge_count; // 边数
        int[,] matrix; // 矩阵内容
        bool haveDirection; // 有方向吗

        public AdjMatrix(AdjMatrix b)
        {
            this.vertex_count = b.vertex_count;
            this.edge_count = b.edge_count;
            this.haveDirection = b.haveDirection;
            this.matrix = new int[b.vertex_count, b.vertex_count];
            for (int i = 0; i < this.vertex_count; i++)
            {
                for (int j = 0; j < this.vertex_count; j++)
                {
                    this.matrix[i, j] = b.matrix[i, j];
                }
            }
        }

        public AdjMatrix(int vertex, bool haveDirection)
        {
            this.vertex_count = vertex;
            this.edge_count = 0;
            this.matrix = new int[vertex, vertex];
            this.haveDirection = haveDirection;
        }

        // 添加边
        public bool AddEdge(int a, int b)
        {
            if (a < 0 || a >= vertex_count || b < 0 || b >= vertex_count)
            {
                return false;
            }

            matrix[a, b] = 1;
            if (!this.haveDirection)
            {
                matrix[b, a] = 1;
            }
            edge_count++;

            return true;
        }

        // 交换行
        public bool ExchangeRow(int a, int b)
        {
            if (a < 0 || a >= vertex_count || b < 0 || b >= vertex_count)
            {
                return false;
            }

            // 行号相同则不进行交换，直接返回
            if (a == b)
            {
                return true;
            }

            int temp;
            for (int j = 0; j < matrix.GetLength(0); j++)
            {
                temp = matrix[a, j];
                matrix[a, j] = matrix[b, j];
                matrix[b, j] = temp;
            }

            return true;
        }

        // 交换两列
        public bool ExchangeColumn(int a, int b)
        {
            if (a < 0 || a >= vertex_count || b < 0 || b >= vertex_count)
            {
                return false;
            }

            // 列号相同则不进行交换，直接返回
            if (a == b)
            {
                return true;
            }

            int temp;
            for (int i = 0; i < matrix.GetLength(1); i++)
            {
                temp = matrix[i, a];
                matrix[i, a] = matrix[i, b];
                matrix[i, b] = temp;
            }

            return true;
        }

        // 判断是否相等
        public override bool Equals(object obj)
        {
            if (obj == null || this.GetType() != obj.GetType())
            {
                return false;
            }

            AdjMatrix b = (AdjMatrix)obj;

            if (this.vertex_count != b.vertex_count || this.edge_count != b.edge_count || this.haveDirection != b.haveDirection)
            {
                return false;
            }

            for (int i = 0; i < this.vertex_count; i++)
            {
                for (int j = 0; j < this.vertex_count; j++)
                {
                    if (this.matrix[i, j] != b.matrix[i, j])
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        // 获得第 i 个节点的度
        public int GetDegree(int i)
        {
            int degree = 0;

            for (int j = 0; j < this.vertex_count; j++)
            {
                degree += this.matrix[i, j];
            }

            return degree;
        }

        // 获得所有节点的度
        public int[] GetEachDegree()
        {
            int[] degrees = new int[this.vertex_count];

            for (int i = 0; i < this.vertex_count; i++)
            {
                degrees[i] = this.GetDegree(i);
            }

            return degrees;
        }

        // 获得第 i 个节点的临近特征串（直接相连的节点的度值按升序排成的字符串）
        public string GetNearInfo(int i)
        {
            StringBuilder sb = new StringBuilder();
            List<int> counts = new List<int>();

            for (int j = 0; j < this.vertex_count; j++)
            {
                if (this.matrix[i, j] != 0)
                {
                    counts.Add(this.GetDegree(j));
                }
            }

            // 排序并转换成字符串
            counts.Sort();
            for (int t = 0; t < counts.Count; t++)
            {
                sb.Append(counts[t]);
            }

            return sb.ToString();
        }

        // 获得所有节点的临近特征串（直接相连的节点的度值按升序排成的字符串）
        public string[] GetEachNearInfo()
        {
            string[] infos = new string[this.vertex_count];
            for (int i = 0; i < this.vertex_count; i++)
            {
                infos[i] = this.GetNearInfo(i);
            }

            return infos;
        }

        // 获得所有节点的的距离特征串（当前节点到其他节点的最短距离按升序排成的字符串）
        public string[] GetEachSmToInfo()
        {
            BreadthFirstSearch bfs;
            List<int> lengths;
            StringBuilder sb;
            string[] lenInfo = new string[this.vertex_count];

            for (int i = 0; i < this.vertex_count; i++)
            {
                lengths = new List<int>();
                bfs = new BreadthFirstSearch(this, i);
                for (int j = 0; j < this.vertex_count; j++)
                {
                    if (this.matrix[i, j] != 0)
                    {
                        lengths.Add(bfs.PathLenTo(j));
                    }
                }

                // 排序并转换成字符串
                lengths.Sort();
                sb = new StringBuilder();
                for (int t = 0; t < lengths.Count; t++)
                {
                    sb.Append(lengths[t]);
                }
                lenInfo[i] = sb.ToString();
            }
            
            return lenInfo;
        }

        // 获得所有节点的路径特征串（当前节点直接相连的节点间的连接信息，k = 2）
        public string[] GetEachPathInfo()
        {
            StringBuilder sb;
            List<int> pathPower;
            string[] pathInfo = new string[this.vertex_count];
            int[,] matrix2 = AdjMatrix.Power(this.matrix, 2);

            for (int i = 0; i < this.vertex_count; i++)
            {
                pathPower = new List<int>();
                for (int j = 0; j < this.vertex_count; j++)
                {
                    pathPower.Add(matrix2[i, j]);
                }

                pathPower.Sort();
                sb = new StringBuilder();
                for (int t = 0; t < pathPower.Count; t++)
                {
                    sb.Append(pathPower[t]);
                }
                pathInfo[i] = sb.ToString();
            }

            return pathInfo;
        }

        // 转换成字符串
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < this.vertex_count; i++)
            {
                for (int j = 0; j < this.vertex_count; j++)
                {
                    sb.Append(this.matrix[i, j] + "\t");
                }
                sb.Append("\n");
            }

            return string.Format("vertex_count: {0},\nedge_count: {1},\nhaveDirection: {2}\n{3}", vertex_count, edge_count, haveDirection, sb.ToString());
        }

        public int[,] GetMatrix()
        {
            return matrix;
        }

        public int GetEdgesCount()
        {
            return edge_count;
        }

        public int GetVertexCount()
        {
            return vertex_count;
        }

        public bool HaveDirection()
        {
            return haveDirection;
        }

        // 乘法运算
        public static int[,] Multiply(int[,] a, int[,] b)
        {
            if (a == null || b == null || a.GetLength(0) != b.GetLength(1))
            {
                Console.WriteLine("parameter error");
                return null;
            }

            int[,] result = new int[a.GetLength(1), b.GetLength(0)];

            for (int i = 0; i < result.GetLength(1); i++)
            {
                for (int j = 0; j < result.GetLength(0); j++)
                {
                    for (int k = 0; k < a.GetLength(0); k++)
                    {
                        result[i, j] += a[i, k] * b[k, j];
                    }
                }
            }

            return result;
        }

        // 获得单位矩阵
        public static int[,] E(int n)
        {
            if (n <= 0)
            {
                return null;
            }

            int[,] e = new int[n, n];
            for (int i = 0; i < n; i++)
            {
                e[i, i] = 1;
            }

            return e;
        }

        // 指数运算
        public static int[,] Power(int[,] a, int power)
        {
            int[,] result;

            if (a == null || a.GetLength(0) != a.GetLength(1))
            {
                return null;
            }

            // power == 0
            if (power == 0)
            {
                return E(a.GetLength(0));
            }

            // power >= 1
            result = a;

            while (power-- > 1)
            {
                result = Multiply(result, a);
            }

            return result;
        }

        public bool IsoEqual(AdjMatrix b)
        {
            // Generate
            int[] array = new int[b.GetVertexCount()];
            for (int i = 0; i < b.GetVertexCount(); i++)
            {
                array[i] = i;
            }
            LittlePermutation lp = new LittlePermutation();
            List<List<int>> ll = lp.GenerateFullPermutation(array, this, b);
            Console.WriteLine("---------------候选匹配-------------");
            for (int i = 0; i < ll.Count; i++)
            {
                for (int j = 0; j < ll[i].Count; j++)
                {
                    Console.Write(ll[i][j] + "\t");
                }
                Console.WriteLine();
            }

            AdjMatrix tb;
            bool[] used;
            for (int i = 0; i < ll.Count; i++)
            {
                tb = new AdjMatrix(b); // 避免 C# 的浅拷贝影响
                used = new bool[this.vertex_count];
                Console.WriteLine("start");
                for (int j = 0; j < ll[i].Count; j++)
                {
                    if (used[j] == true && used[ll[i][j]] == true)
                    {
                        Console.WriteLine("EX {0}, {1} used", j, ll[i][j]);
                    }
                    else
                    {
                        Console.WriteLine("EX {0}, {1} using", j, ll[i][j]);
                        tb.ExchangeColumn(j, ll[i][j]);
                        tb.ExchangeRow(j, ll[i][j]);
                        used[j] = true;
                        used[ll[i][j]] = true;
                    }
                }
                
                if (this.Equals(tb))
                {
                    Console.WriteLine("find match " + i);
                    return true;
                }
            }

            return false;
        }
    }
}
